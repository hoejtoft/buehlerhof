<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_service', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Service_headline_de');
            $table->string('Service_headline_en');
            $table->string('Service_headline_it');
            $table->string('Service_content_de');
            $table->string('Service_content_en');
            $table->string('Service_content_it');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_service');
    }
}
