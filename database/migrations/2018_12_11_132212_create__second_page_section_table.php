<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondPageSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_second_page_section', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->string('type');
            $table->string('headline_de');
            $table->string('headline_en');
            $table->string('headline_it');
            $table->string('sectiontxt_de');
            $table->string('sectiontxt_en');
            $table->string('sectiontxt_it');
            $table->string('alltxt_de');
            $table->string('alltxt_en');
            $table->string('alltxt_it');
            $table->integer('hasbutton');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_second_page_section');
    }
}
