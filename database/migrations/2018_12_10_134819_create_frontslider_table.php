<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrontsliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frontslider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imgurl');
            $table->string('teasertext_de');
            $table->string('teasertext_en');
            $table->string('teasertext_it');
            $table->string('buttontext_de');
            $table->string('buttontext_en');
            $table->string('buttontext_it');
            $table->string('buttonlink');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frontslider');
    }
}
