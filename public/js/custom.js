$(document).ready(function () {
    SmallMenuOnScroll();
    floatLabel(".floatLabel");
    showPreImage();
    displayImgUploads();
    imgUpload();
    initFloatLabels();
    ValidateDelete();
    ToggleScrollTop();
    ScrollToTop();
    setSlideActive();
    updateSelect();
    clearImage();
    setButtonvisibility();
    setTextvisibility();
    initSlickslider();
    setStarrating();
    initContextMenu();
    $('[data-toggle="tooltip"]').tooltip();
    $('.navbar-light .dmenu').hover(function () {
        $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
    }, function () {
        $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
    });
});

function SmallMenuOnScroll(){
    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        if(scroll>137){
            $("#logo").addClass("halfsize");
            $(".navbar").addClass("positionFixed");
            $(".container-top").css("margin-top","230px");
        }
        else{
            $(".container-top").css("margin-top","0px");
            $("#logo").removeClass("halfsize");
            $(".navbar").removeClass("positionFixed");
        }
    });
}


    function floatLabel(inputType){
        $(inputType).each(function(){
            var $this = $(this);
            var text_value = $(this).val();

            // on focus add class "active" to label
            $this.focus(function(){
                $this.next().addClass("active");
            });

            // on blur check field and remove class if needed
            $this.blur(function(){
                if($this.val() === '' || $this.val() === 'blank'){
                    $this.next().removeClass();
                }
            });

            // Check input values on postback and add class "active" if value exists
            if(text_value!=''){
                $this.next().addClass("active");
            }
        });

        // Automatically remove floatLabel class from select input on load
        $( "select" ).next().removeClass();
    }
function showPreImage(){
    $('[id^=imgname_]').change( function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        var placeholder = $(this).next().attr("id");
        //var fadeimg = $(this).attr("id");
        //$("#"+fadeimg).css("opacity","0.3");
        $("#"+placeholder+"").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));

        $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["+tmppath+"]</strong>");
    });
}
function displayImgUploads(){
    $("#sectiontype").change(function(){
       var val = $(this).val();
        if(val == 0){
            $("#imgname_1").fadeIn(100);
            $("#imgname_2").fadeOut(100);
            $("#imgname_3").fadeOut(100);
            $("#imgname_4").fadeOut(100);
            $("#imgname_5").fadeOut(100);
        }
        else if(val == 1){
            $("#imgname_1").fadeIn(100);
            $("#imgname_2").fadeIn(100);
            $("#imgname_3").fadeIn(100);
            $("#imgname_4").fadeIn(100);
            $("#imgname_5").fadeIn(100);
        }
        else{
            $("#imgname_1").fadeOut(100);
            $("#imgname_2").fadeOut(100);
            $("#imgname_3").fadeOut(100);
            $("#imgname_4").fadeOut(100);
            $("#imgname_5").fadeOut(100);
        }
        console.log(val);
    });
}

function imgUpload(){
    $('[id^=imgname_]').on("change", function() {
        var element = $(this).attr("id");
        var imgurl = $(this).next().next().attr("id");
        var loader = $(this).next().next().next().attr("id");
        console.log(element);
        console.log(imgurl);
        console.log(loader);
        $("#"+loader).fadeIn(0);
        $("#"+element).fadeOut(0);
        var $files = $(this).get(0).files;

        if ($files.length) {

            // Reject big files
            if ($files[0].size > $(this).data("max-size") * 1024) {
                console.log("Please select a smaller file");
                return false;
            }
            // Begin file upload
            console.log("Uploading file to Imgur..");

            //client secret: 4a0987af61dc3729a7bc12fec1da450cf034e288
            // Replace ctrlq with your own API key
            var apiUrl = 'https://api.imgur.com/3/image';
            var apiKey = '76c69cd072e2e2a';
            var albumId = "bO8YKlM";

            var settings = {
                async: true,
                crossDomain: true,
                processData: false,
                contentType: false,
                type: 'POST',
                url: apiUrl,
                headers: {
                    Authorization: 'Client-ID ' + apiKey,
                    Accept: 'application/json'
                },
                mimeType: 'multipart/form-data'
            };

            var formData = new FormData();
            formData.append("image", $files[0]);
            settings.data = formData;

            // Response contains stringified JSON
            // Image URL available at response.data.link
            $.ajax(settings).done(function(response) {
                var obj = JSON.parse(response);
                console.log(obj['data']);
                $("#"+imgurl).val(obj.data.link);
                $("#"+loader).fadeOut(0);
                $("#"+element).fadeIn(0);


            });

        }
    });
}
function initFloatLabels(){
    $("#sectiontype_label").addClass("active");
    $("#category_label").addClass("active");
    $("#type_label").addClass("active");
}
function ValidateDelete(){
    $('.delete_form').on('submit',function(){
        if(confirm("Do you want to delete the post?")){
            return true;
        }
        else{
            return false;
        }
    });
}
function ToggleScrollTop(){
    $(window).scroll(function() {
        var height = $(window).scrollTop();

        if(height  > 100) {
            $("#to-top").fadeIn(100);
        }
        else{
            $("#to-top").fadeOut(100);
        }
    });
}
function ScrollToTop(){
    $("#to-top").click(function(){
        $('html').animate({scrollTop:0}, 'slow');//IE, FF
        $('body').animate({scrollTop:0}, 'slow');//chrome, don't know if Safari works
    });
}
function setSlideActive(){
    $('.slideindicator_0').addClass("active");
    $(".slideitem_0").addClass("active");
}
function updateSelect(){
    $("#update_selectbox").click(function(){
        $("#sectiontype").trigger("change");
    });
}
function clearImage(){
    $(".del_pic").click(function(){
        var imageinput = $(this).prev().prev().attr("id");
        var imagepreview = $(this).prev().prev().prev().attr("id");
        $("#"+imageinput).val("");
        $("#"+imagepreview).fadeOut(100);
        $(this).fadeOut(100);
        console.log(imageinput);
        console.log(imagepreview);

    });
}
function setButtonvisibility(){
    $("#buttononnoff").val(0);
    $("#switch").click(function(){
     var onoff = $("#switch").prop("checked");
        if(onoff == true){
            $("#buttononnoff").val(1);
        }
        else{
            $("#buttononnoff").val(0);

        }
    });
}
function setTextvisibility(){
    $("#textonnoff").val(0);
    $("#switch").click(function(){
        var onoff = $("#switch").prop("checked");
        if(onoff == true){
            $("#textonnoff").val(1);
        }
        else{
            $("#textonnoff").val(0);

        }
    });
}
function initSlickslider(){
    $('.customer-logos').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });
}

function setStarrating(){
    $(':radio').change(function() {
        $("#starsinput").val(this.value);
    });
}
function initContextMenu(){
    $(".btn-newline").on('click', function() {
        var textfield = $(this).prev().prev().attr("id");
        var $txt = $("#"+textfield);
        var caretPos = $txt[0].selectionStart;
        var textAreaTxt = $txt.val();
        var txtToAdd = "<br><br>";
        $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
    });
    $(".btn-headline").on('click', function() {
        var textfield = $(this).prev().prev().prev().attr("id");
        var $txt = $("#"+textfield);
        var caretPos = $txt[0].selectionStart;
        var textAreaTxt = $txt.val();
        var txtToAdd = "<h3></h3>";
        $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
    });
    $(".btn-bold").on('click', function() {
        var textfield = $(this).prev().prev().prev().prev().attr("id");
        var $txt = $("#"+textfield);
        var caretPos = $txt[0].selectionStart;
        var textAreaTxt = $txt.val();
        var txtToAdd = "<b></b>";
        $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
    });

}


