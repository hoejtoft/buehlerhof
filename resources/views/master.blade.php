<?php
$lang = "";
if(Session::has('lang')){
    $lang = Session::get('lang');
}
else{
    $lang = "de";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta property="og:title" content="Home"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://www.buehlerhof.it/"/>
    <meta property="og:site_name" content="Buehlerhof"/>
    <meta property="og:description" content="BÜHLERHOF
NATUR MIT ALLEN SINNEN ERLEBEN
EIN ORT AN DEM NATUR GREIFBAR UND LEBENDIG IST.
EIN ORT DER ENTSCHLEINIGT, ERDET UND BERUHIGT.
EIN ORT DER TRADITION, BRAUCHTUM UND MODERNE VEREINT.
ZUKUNFT BRAUCHT HERKUNFT
MIT VIEL HERZLICHKEIT UND SÜDTIROLER GASTFREUNDSCHAFT HEISSEN WIR SIE AM BÜHLERHOF IN RAAS WILLKOMMEN. JENEM"/>
    <meta name="robots" content="index,follow" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/slick.js') }}"></script>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">


</head>
<body>
<nav class="navbar navbar-expand-md    navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{url('/')}}">
        <img id="logo" src="{{ asset('/img/logo.jpg') }}">
    </a>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

            <li class="nav-item dropdown dmenu">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Hof
                </a>
                <div class="dropdown-menu sm-menu">
                    <a class="dropdown-item" href="{{url('/Hof')}}">Bühlerhof</a>
                    <a class="dropdown-item" href="{{url('/Engagement')}}">Engagement</a>
                    <a class="dropdown-item" href="{{url('/Anreise')}}">Anreise</a>
                </div>
            </li>
            <li class="nav-item dropdown dmenu">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Urlaub
                </a>
                <div class="dropdown-menu sm-menu">
                    <a class="dropdown-item" href="{{url('/Urlaub')}}">Urlauber</a>
                    <a class="dropdown-item" href="{{url('/Wohnen')}}">Wohnen</a>
                    <a class="dropdown-item" href="{{url('/Wellness')}}">Wellness</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/Kulinarik')}}">Kulinarik</a>
            </li>
            <li class="nav-item dropdown dmenu">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Aktiv sein
                </a>
                <div class="dropdown-menu sm-menu">
                    <a class="dropdown-item" href="{{url('/Aktivsein')}}">Aktiv sein</a>
                    <a class="dropdown-item" href="{{url('/Erde')}}">Erde</a>
                    <a class="dropdown-item" href="{{url('/Wasser')}}">Wasser</a>
                    <a class="dropdown-item" href="{{url('/Luft')}}">Luft</a>
                    <a class="dropdown-item" href="{{url('/Feuereis')}}">Feuer Und Eis</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/Sydtirol')}}">Südtirol</a>
            </li>
            {{--@if(auth()->guest())--}}
            {{--make admin later--}}
                @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Request::is('addsection'))
                    Add section
                    @else
                    Admin panel
                    @endif
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="nav-link dropdown-item" href="{{ url('/addsection') }}">Add new page section</a>
                    <a class="nav-link dropdown-item" href="{{ url('/addother') }}">Add other section types</a>
                    <a class="nav-link dropdown-item" href="{{ url('/editslider') }}">Add or edit front slider</a>
                    <a class="nav-link dropdown-item" href="{{ url('/addreview') }}">Add or edit review</a>
                    <a class="nav-link dropdown-item" href="{{ url('/addservicesection') }}">Add & edit service content</a>

                </div>
            </div>
                @endif
                @guest
                {{--<li class="nav-item {{ Request::is('login') ? 'active' : '' }}">--}}
                    {{--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                {{--</li>--}}
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
            <div class="lang-section">
                <li class="nav-item">
                    <a class="nav-link {{Session::get('lang') == "en" ? "activelang" : ""}}" href="{{ url('/en') }}">En</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{Session::get('lang') == "de" ? "activelang" : ""}}" href="{{ url('/de') }}">De</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{Session::get('lang') == "it" ? "activelang" : ""}}" href="{{ url('/it') }}">It</a>
                </li>
            </div>
        </ul>
    </div>

</nav>
<div class="line-split"></div>

@yield('indexcontent')
@yield('content')


<!-- Footer -->
<div class="line-split"></div>
<section id="footer">

    <div class="container">
        <div class="row text-center text-xs-center text-sm-left text-md-left">
            <div class="col-xs-12 col-sm-3 col-md-3">
                <h5>Anschrift</h5>
                Obst & Weingut Bühlerhof <br>
                Familie Nicole & August Thaler <br>
                Unterrainerstr. 9-9 a <br>
                39040 Natz-Schabs (Raas) <br>
                Südtirol / Italien <br>
                Hinweis: bei Neustift / Brixen
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <h5>Tel | Mail</h5>
                <i class="fas fa-phone"></i> Nicole: +39 389 56 87 250
                <br>
                <i class="fas fa-phone"></i> August: +39 342 80 23 555
                <br>
                <i class="fas fa-envelope"></i> info@buehlerhof.it
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <h5>Quick links</h5>
                <ul class="list-unstyled quick-links">
                    <li><a href="{{url('/')}}"><i class="fa fa-angle-double-right"></i>Home</a></li>
                    <li><a href="{{url('/login')}}"><i class="fa fa-angle-double-right"></i>Login</a></li>
                    <li><a href="{{url('/Anreise')}}"><i class="fa fa-angle-double-right"></i>Anreise</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <h5>Service</h5>
                <ul class="list-unstyled quick-links">
                    @foreach($Servicesection as $row)
                        <li><div class="modallink" data-toggle="modal" data-target="#Servicemodal_view{{$row->id}}">
                                <i class="fa fa-angle-double-right"></i>
                                @if($lang == "en")
                                    {{$row->Service_headline_en}}
                                @elseif($lang == "de")
                                    {{$row->Service_headline_de}}
                                @else
                                    {{$row->Service_headline_it}}
                                @endif
                            </div></li>

                            <!-- Modal -->
                            <div class="modal fade" id="Servicemodal_view{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLabel">
                                                @if($lang == "en")
                                                    {{$row->Service_headline_en}}
                                                @elseif($lang == "de")
                                                    {{$row->Service_headline_de}}
                                                @else
                                                    {{$row->Service_headline_it}}
                                                @endif
                                            </h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            @if($lang == "en")
                                                {!!$row->Service_content_en!!}
                                            @elseif($lang == "de")
                                                {!!$row->Service_content_de!!}
                                            @else
                                                {!!$row->Service_content_it!!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End modal -->
                            @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                <ul class="list-unstyled list-inline social text-center">
                    <li class="list-inline-item"><a href="https://www.facebook.com/buehlerhof.it/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    {{--<li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-twitter"></i></a></li>--}}
                    <li class="list-inline-item"><a href="https://www.instagram.com/19buehlerhof05/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                    {{--<li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>--}}
                    <li class="list-inline-item"><img width="100%" id="footer_hahn" src="{{ asset('/img/RoterHahn.gif') }}"></li>
                </ul>
            </div>
            </hr>
        </div>
    </div>
        <div class="line-split"></div>
        <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                <img width="100%" id="footer_tyrol" src="{{ asset('/img/tyrol.png') }}">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                <p>&copy OBST & WEINGUT BÜHLERHOF</p>
            </div>
            </hr>
        </div>
    </div>
</section>
<!-- ./Footer -->
<div id="to-top"><i class="fas fa-chevron-up"></i></div>
</body>
</html>


