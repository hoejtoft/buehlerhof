@extends('master')
@section('title', 'Buehlerhof - Manage slider')
@section('content')
    <div class="container container-top">
        <div class="row">

            <form method="post" enctype="multipart/form-data" action="{{action('SlideController@update', $id)}}">
                {{csrf_field()}}
                {{ method_field('PUT')}}
                <h1 class="text-center Headline">Edit</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif

                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">Add new slider object</h2>

                    <div class="controls third">
                        <textarea name="teasertext_de"  class="floatLabel" id="teasertext_de">{{ $slideobj->teasertext_de }}</textarea>
                        <label for="teasertext_de">German teaser text</label>
                    </div>
                    <div class="controls third">
                        <textarea name="teasertext_en" class="floatLabel" id="teasertext_en">{{ $slideobj->teasertext_en }}</textarea>
                        <label for="teasertext_en">English teaser text</label>
                    </div>
                    <div class="controls third">
                        <textarea name="teasertext_it" class="floatLabel" id="teasertext_it">{{ $slideobj->teasertext_it }}</textarea>
                        <label for="teasertext_it">Italian teaser text</label>
                    </div>
                    <div class="controls third">
                        <input type="text" name="buttontext_de" class="floatLabel" id="buttontext_de" value="{{ $slideobj->buttontext_de }}">
                        <label for="buttontext_de">German button text</label>
                    </div>
                    <div class="controls third">
                        <input type="text" name="buttontext_en" class="floatLabel" id="buttontext_en" value="{{ $slideobj->buttontext_en }}">
                        <label for="buttontext_en">English button text</label>
                    </div>
                    <div class="controls third">
                        <input type="text" name="buttontext_it" class="floatLabel" id="buttontext_it" value="{{ $slideobj->buttontext_it }}">
                        <label for="buttontext_it">Italian button text</label>
                    </div>
                    <div class="controls full">
                        <div >Set text visibility</div>
                        <div class="switchToggle">
                            <input type="checkbox" id="switch">
                            <label for="switch">Toggle</label>
                            <input type="hidden" id="textonnoff" name="textvisible">
                        </div>
                        @if($slideobj->textvisible == 1)
                            <script>
                                $(document).ready(function(){
                                    $("#textonnoff").val(1);
                                    $("#switch").click();
                                });
                            </script>
                        @endif
                    </div>
                    <div class="controls full">
                        <select class="floatLabel" name="buttonlink" id="buttonlink">
                            <option value="blank"></option>
                            <option value="/Hof" {{($slideobj->buttonlink == '/Hof') ? 'selected' : ''}}>Hof</option>
                            <option value="/Engagement" {{($slideobj->buttonlink == '/Engagement') ? 'selected' : ''}}>Engagement</option>
                            <option value="/Urlauber" {{($slideobj->buttonlink == '/Urlauber') ? 'selected' : ''}}>Urlauber</option>
                            <option value="/Wohnen" {{($slideobj->buttonlink == '/Wohnen') ? 'selected' : ''}}>Wohnen</option>
                            <option value="/Kulinarik" {{($slideobj->buttonlink == '/Kulinarik') ? 'selected' : ''}}>Kulinarik</option>
                            <option value="/Aktivsein" {{($slideobj->buttonlink == '/Aktivsein') ? 'selected' : ''}}>Aktiv sein</option>
                            <option value="/Wellness" {{($slideobj->buttonlink == '/Wellness') ? 'selected' : ''}}>Wellness</option>
                            <option value="/Sydtyrol" {{($slideobj->buttonlink == '/Sydtyrol') ? 'selected' : ''}}>Südtyrol</option>
                        </select>
                        <label for="buttonlink">Select button link</label>
                    </div>
                </div>
                <!--  Details -->
                <div class="form-group">
                    <h2 class="heading full">Add image</h2>

                    <div class="controls full">
                        <input type="file" name="imgname_1" id="imgname_1"  class="filestyle" style="display:block;">
                        <img width="100%" class="imgpreview" id="tempimg1" src="{{$slideobj->imgurl}}" style="display:block;">
                        <input type="hidden" name="imgurl_1" id="imgurl_1" value="{{$slideobj->imgurl}}">
                        <div class="loader" id="loader_1" style="display: none;"></div>
                    </div>
                    <div class="controls full">
                        <button class="full">Update slider object</button>
                    </div>
                </div>

            </form>
        </div>

    </div>
@endsection