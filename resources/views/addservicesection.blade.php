@extends('master')
@section('title', 'Buehlerhof - add service section')
@section('content')
    <div class="container container-top">
        <div class="row">

            <form method="post" enctype="multipart/form-data" action="{{url('addservice')}}">
                {{csrf_field()}}
                <h1 class="text-center Headline">Add new service section</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif

                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">Section content</h2>
                    <div class="controls full">
                        <input type="text" id="Service_headline_de" value="{{ old('Service_headline_de') }}" class="floatLabel" name="Service_headline_de">
                        <label for="Service_headline_de">German service headline</label>
                    </div>
                    <div class="controls full">
                        <textarea name="Service_content_de"  class="floatLabel" id="Service_content_de">{{ old('Service_content_de') }}</textarea>
                        <label for="Service_content_de">German service content</label>
                        <input type="button" class="btn-newline btn-dark" value="Add new line" />
                        <input type="button" class="btn-headline btn-dark" value="Add head line" />
                        <input type="button" class="btn-bold btn-dark" value="Add bold" />
                    </div>
                    <div class="controls full">
                        <input type="text" id="Service_headline_en" value="{{ old('Service_headline_en') }}" class="floatLabel" name="Service_headline_en">
                        <label for="Service_headline_en">English service headline</label>
                    </div>
                    <div class="controls full">
                        <textarea name="Service_content_en"  class="floatLabel" id="Service_content_en">{{ old('Service_content_en') }}</textarea>
                        <label for="Service_content_en">English service content</label>
                        <input type="button" class="btn-newline btn-dark" value="Add new line" />
                        <input type="button" class="btn-headline btn-dark" value="Add head line" />
                        <input type="button" class="btn-bold btn-dark" value="Add bold" />
                    </div>
                    <div class="controls full">
                        <input type="text" id="Service_headline_it" value="{{ old('Service_headline_it') }}" class="floatLabel" name="Service_headline_it">
                        <label for="Service_headline_it">Italian service headline</label>
                    </div>
                    <div class="controls full">
                        <textarea name="Service_content_it"  class="floatLabel" id="Service_content_it">{{ old('Service_content_it') }}</textarea>
                        <label for="Service_content_it">Italian service content</label>
                        <input type="button" class="btn-newline btn-dark" value="Add new line" />
                        <input type="button" class="btn-headline btn-dark" value="Add head line" />
                        <input type="button" class="btn-bold btn-dark" value="Add bold" />
                    </div>
                    <div class="controls full">
                        <button class="full">Add service section</button>
                    </div>
                </div>

            </form>
            <h2 class="heading full">Service contents</h2>
            <table class="table table-bordered table-striped custom-table-responsive">
                <tr>
                    <th>Service_headline_de</th>
                    <th>Service_headline_en</th>
                    <th>Service_headline_it</th>
                    <th>Service_content_de</th>
                    <th>Service_content_en</th>
                    <th>Service_content_it</th>
                    <th>Created at:</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                @foreach($Servicesection as $row)
                    <tr>
                        <td>{{$row->Service_headline_de}}</td>
                        <td>{{$row->Service_headline_en}}</td>
                        <td>{{$row->Service_headline_it}}</td>
                        <td>{{$row->Service_content_de}}</td>
                        <td>{{$row->Service_content_en}}</td>
                        <td>{{$row->Service_content_it}}</td>
                        <td>{{$row->created_at}}</td>
                        <td><a class="btn btn-warning" href="{{action('ServiceController@edit',$row->id)}}">Edit</a></td>
                        <td>
                            <form  method="post" class="delete_form reset-this" action="{{action('ServiceController@destroy', $row->id)}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE" />
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection