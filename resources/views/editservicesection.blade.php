@extends('master')
@section('title', 'Buehlerhof - edit service section')
@section('content')
    <?php
    $lang = "";
    if(Session::has('lang')){
        $lang = Session::get('lang');
    }
    else{
        $lang = "en";
    }
    ?>
    <div class="container container-top">
        <div class="row">

            <form method="post" enctype="multipart/form-data" action="{{action('ServiceController@update', $id)}}">
                {{csrf_field()}}
                {{ method_field('PUT')}}
                <h1 class="text-center Headline">Edit & update service section</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif

                <!--  General -->

                <div class="form-group">
                    <h2 class="heading full">Section content</h2>
                    <div class="controls full">
                        <input type="text" id="Service_headline_de" value="{{ $servicesection->Service_headline_de }}" class="floatLabel" name="Service_headline_de">
                        <label for="Service_headline_de">German service headline</label>
                    </div>

                    <div class="controls full">
                        <textarea name="Service_content_de"  class="floatLabel" class="Service_content_de" id="Service_content_de">{{ $servicesection->Service_content_de }}</textarea>
                        <label for="Service_content_de">German service content</label>
                        <input type="button" class="btn-newline btn-dark" value="Add new line" />
                        <input type="button" class="btn-headline btn-dark" value="Add head line" />
                        <input type="button" class="btn-bold btn-dark" value="Add bold" />
                    </div>
                    <div class="controls full">
                        <input type="text" id="Service_headline_en" value="{{ $servicesection->Service_headline_en }}" class="floatLabel" name="Service_headline_en">
                        <label for="Service_headline_en">English service headline</label>
                    </div>
                    <div class="controls full">
                        <textarea name="Service_content_en"  class="floatLabel" id="Service_content_en">{{ $servicesection->Service_content_en }}</textarea>
                        <label for="Service_content_en">English service content</label>
                        <input type="button" class="btn-newline btn-dark" value="Add new line" />
                        <input type="button" class="btn-headline btn-dark" value="Add head line" />
                        <input type="button" class="btn-bold btn-dark" value="Add bold" />
                    </div>
                    <div class="controls full">
                        <input type="text" id="Service_headline_it" value="{{ $servicesection->Service_headline_it }}" class="floatLabel" name="Service_headline_it">
                        <label for="Service_headline_it">Italian service headline</label>
                    </div>
                    <div class="controls full">
                        <textarea name="Service_content_it"  class="floatLabel" id="Service_content_it">{{ $servicesection->Service_content_it }}</textarea>
                        <label for="Service_content_it">Italian service content</label>
                        <input type="button" class="btn-newline btn-dark" value="Add new line" />
                        <input type="button" class="btn-headline btn-dark" value="Add head line" />
                        <input type="button" class="btn-bold btn-dark" value="Add bold" />
                    </div>
                    <div class="controls full">
                        <button class="full">Update service section</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection