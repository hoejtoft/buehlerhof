@extends('master')
@section('title', 'Buehlerhof')
@section('indexcontent')
    <?php
    $lang = "";
    if(Session::has('lang')){
        $lang = Session::get('lang');
    }
    else{
        $lang = "de";
    }
    ?>
    <div id="carouselExampleIndicators_front" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            @for($i = 0; $i<count($Sliderobj);$i++)
            <li data-target="#carouselExampleIndicators_front" class="slideindicator_{{$i}}" data-slide-to="{{$i}}"></li>
            @endfor
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            @for($i = 0; $i<count($Sliderobj);$i++)
            <div class="carousel-item slideitem_{{$i}}" style="background-image: url('{{ $Sliderobj[$i]->imgurl }}')">
                <div class="carousel-caption d-none d-md-block">

                    <p class="lead-slider" style="display: {{$Sliderobj[$i]->textvisible == 1 ? "block;" : "none;"}}">
                        @if($lang == "en")
                            {{$Sliderobj[$i]->teasertext_en}}
                            @elseif($lang == "de")
                            {{$Sliderobj[$i]->teasertext_de}}
                            @else
                            {{$Sliderobj[$i]->teasertext_it}}
                        @endif
                    </p>
                    <div class="btn btn-cta">
                        <a href="{{$Sliderobj[$i]->buttonlink}}">
                            @if($lang == "en")
                                {{$Sliderobj[$i]->buttontext_en}}
                            @elseif($lang == "de")
                                {{$Sliderobj[$i]->buttontext_de}}
                            @else
                                {{$Sliderobj[$i]->buttontext_it}}
                            @endif
                        </a></div>
                </div>
            </div>
            @endfor
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators_front" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators_front" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    {{--Other section type goes here--}}
    <div class="row no-gutters">
    <div class="SecondaryContent">
    @foreach($secondpagecontent as $secondrow)
    @if($secondrow->type == 0)
        <div class="quarter">
            <div class="othercontent text-center">
                <h5>
                    @if($lang == "en")
                            {{$secondrow->headline_en}}
                        @elseif($lang == "de")
                            {{$secondrow->headline_de}}
                        @else
                            {{$secondrow->headline_it}}
                        @endif
                </h5>
                <p>
                @if($lang == "en")
                            {{$secondrow->sectiontxt_en}}
                        @elseif($lang == "de")
                            {{$secondrow->sectiontxt_de}}
                        @else
                            {{$secondrow->sectiontxt_it}}
                        @endif
                </p>
                @if($secondrow->hasbutton == 1)
                    <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$secondrow->id}}">
                        @if($lang == "en")
                            Read more
                        @elseif($lang == "de")
                            Weiterlesen
                        @else
                            Leggi di più
                        @endif
                    </div>
                @endif
            </div>
            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok other_content_crud">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SecondPageController@destroy', $secondrow->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$secondrow->id}}" href="{{action('SecondPageController@edit',$secondrow->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
        </div>

    @elseif($secondrow->type == 1)
        <div class="third">
            <div class="othercontent text-center">
                <h5>
                @if($lang == "en")
                            {{$secondrow->headline_en}}
                        @elseif($lang == "de")
                            {{$secondrow->headline_de}}
                        @else
                            {{$secondrow->headline_it}}
                        @endif
                </h5>
                <p>
                @if($lang == "en")
                            {{$secondrow->sectiontxt_en}}
                        @elseif($lang == "de")
                            {{$secondrow->sectiontxt_de}}
                        @else
                            {{$secondrow->sectiontxt_it}}
                        @endif
                </p>
                @if($secondrow->hasbutton == 1)
                    <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                        @if($lang == "en")
                            Read more
                        @elseif($lang == "de")
                            Weiterlesen
                        @else
                            Leggi di più
                        @endif
                    </div>
                @endif
            </div>
            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok other_content_crud">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SecondPageController@destroy', $secondrow->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$secondrow->id}}" href="{{action('SecondPageController@edit',$secondrow->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
        </div>
    @elseif($secondrow->type == 2)
        <div class="half">
            <div class="othercontent text-center">
                <h5>
                @if($lang == "en")
                            {{$secondrow->headline_en}}
                        @elseif($lang == "de")
                            {{$secondrow->headline_de}}
                        @else
                            {{$secondrow->headline_it}}
                        @endif
                </h5>
                <p>
                @if($lang == "en")
                            {{$secondrow->sectiontxt_en}}
                        @elseif($lang == "de")
                            {{$secondrow->sectiontxt_de}}
                        @else
                            {{$secondrow->sectiontxt_it}}
                        @endif
                </p>
                @if($secondrow->hasbutton == 1)
                    <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$secondrow->id}}">
                        @if($lang == "en")
                            Read more
                        @elseif($lang == "de")
                            Weiterlesen
                        @else
                            Leggi di più
                        @endif
                    </div>
                @endif
            </div>
            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok other_content_crud">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SecondPageController@destroy', $secondrow->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$secondrow->id}}" href="{{action('SecondPageController@edit',$secondrow->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
        </div>
    @endif
            <!-- Modal -->
                <div class="modal fade" id="modal_view{{$secondrow->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                    @if($lang == "en")
                                        {{$secondrow->headline_en}}
                                    @elseif($lang == "de")
                                        {{$secondrow->headline_de}}
                                    @else
                                        {{$secondrow->headline_it}}
                                    @endif
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                @if($lang == "en")
                                    {{$secondrow->alltxt_en}}
                                @elseif($lang == "de")
                                    {{$secondrow->alltxt_de}}
                                @else
                                    {{$secondrow->alltxt_it}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End modal -->
    @endforeach
    </div>
    </div>
    <section class="showcase">
        <div class="container-fluid p-0">
            @foreach($PageSection as $row)
            @if($row->type == 3)
                 <div class="row no-gutters pagesection">
                        <div class="col-lg-12 my-auto showcase-img" style="background-image: url('{{ $row->imgname_1 }}');">
                            @if($lang == "en")
                                <h2 class="section-img-full">{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2 class="section-img-full">{{$row->headline_de}}</h2>
                            @else
                                <h2 class="section-img-full">{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead mb-0 text-center full-img-p">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
                @elseif($row->type == 1)
                    <div class="row no-gutters pagesection">

                        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('{{ $row->imgname_1 }}');"></div>
                        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                            @if($lang == "en")
                                <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2>{{$row->headline_de}}</h2>
                            @else
                                <h2>{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead mb-0">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
                @elseif($row->type == 0)
                    <div class="row no-gutters pagesection">
                        <div class="col-lg-6 text-white showcase-img" style="background-image: url('{{ $row->imgname_1 }}');"></div>
                        <div class="col-lg-6 my-auto showcase-text">
                            @if($lang == "en")
                                <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2>{{$row->headline_de}}</h2>
                            @else
                                <h2>{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead mb-0">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
                @else
                {{--no image full width--}}
                    <div class="row no-gutters pagesection">
                        <div class="col-lg-12 my-auto showcase-text">
                            @if($lang == "en")
                                <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2>{{$row->headline_de}}</h2>
                            @else
                                <h2>{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead mb-0 text-center">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
                    @endif
                            <!-- Modal -->
                    <div class="modal fade" id="modal_view{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        @if($lang == "en")
                                            {{$row->headline_en}}
                                        @elseif($lang == "de")
                                            {{$row->headline_de}}
                                        @else
                                            {{$row->headline_it}}
                                        @endif
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @if($lang == "en")
                                        {{$row->alltxt_en}}
                                    @elseif($lang == "de")
                                        {{$row->alltxt_de}}
                                    @else
                                        {{$row->alltxt_it}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End modal -->
                    @endforeach

        </div>
    </section>

    {{--end escond content--}}
    <div class="row no-gutters">
        <div class="container">
            <section class="customer-logos slider">
                @foreach($Reviews as $review)
                    <div class="slide">
                        <div class="review-item">
                        <div class="review-headline">
                            @if($lang == "en")
                                Review from {{$review->sourcewebsite}}
                            @elseif($lang == "de")
                                Bewertung von {{$review->sourcewebsite}}
                            @else
                                Recensione da {{$review->sourcewebsite}}
                            @endif
                        </div>
                        <div class="review-text">
                            @if($lang == "en")
                                 {{$review->reviewtext_en}}
                            @elseif($lang == "de")
                                {{$review->reviewtext_de}}
                            @else
                                {{$review->reviewtext_it}}
                            @endif
                        </div>
                        <div class="review-rating">
                            @if($review->stars == 1)
                            <div class="stars">
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                        </div>
                            @elseif($review->stars == 2)
                            <div class="stars">
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                        </div>
                            @elseif($review->stars == 3)
                            <div class="stars">
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                        </div>
                            @elseif($review->stars == 4)
                            <div class="stars">
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="fas fa-star" aria-hidden="true"></i>
                            <i class="far fa-star" aria-hidden="true"></i>
                        </div>
                            @elseif($review->stars == 5)
                                <div class="stars">
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                </div>
                            @endif

                        </div>
                        </div>
                    </div>
                @endforeach
            </section>
        </div>
    </div>
    {{--<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAOzaHaTydR6PRX-nkiWcX6DBlrxpnGDS0'></script><div style='overflow:hidden;height:400px;width:100%;'><div id='gmap_canvas' style='height:400px;width:100%;'></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=280ac9d5935b65e9bc121d00c34e2b11d656aa96'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:13,center:new google.maps.LatLng(46.7466124,11.652795200000014),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(46.7466124,11.652795200000014)});infowindow = new google.maps.InfoWindow({content:'<strong> Unterrainerstraße 9, Natz-Schabs, Bozen, Italien</strong><br> Unterrainerstraße 9, Natz-Schabs, Bozen, Italien<br> <br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>--}}
@endsection