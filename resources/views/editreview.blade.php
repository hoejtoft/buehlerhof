@extends('master')
@section('title', 'Buehlerhof - Edit review')
@section('content')
    <div class="container container-top">
        <div class="row">

            <form method="post" enctype="multipart/form-data" action="{{action('ReviewController@update', $id)}}">
                {{csrf_field()}}
                {{ method_field('PUT')}}
                <h1 class="text-center Headline">Edit review</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif

                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">Review content</h2>
                    <div class="controls full">
                        <input type="text" id="Sourcewebsite" value="{{ $review->sourcewebsite }}" class="floatLabel" name="Sourcewebsite">
                        <label for="Sourcewebsite">Write review source website</label>
                    </div>

                    <div class="controls third">
                        <textarea name="reviewtext_de" class="floatLabel" id="reviewtext_de">{{ $review->reviewtext_de }}</textarea>
                        <label for="reviewtext_de">German review text</label>
                    </div>
                    <div class="controls third">
                        <textarea name="reviewtext_en" class="floatLabel" id="reviewtext_en">{{ $review->reviewtext_en }}</textarea>
                        <label for="reviewtext_en">English review text</label>
                    </div>
                    <div class="controls third">
                        <textarea name="reviewtext_it" class="floatLabel" id="reviewtext_it">{{ $review->reviewtext_it }}</textarea>
                        <label for="reviewtext_it">Italian review text</label>
                    </div>
                </div>
                <!--  Details -->
                <div class="form-group">
                    <h2 class="heading full">Review rating </h2>
                    <div class="rating">
                        <label>
                            <input type="radio" name="stars" value="1" />
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="2" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="3" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="4" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="5" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                    </div>
                    <input type="hidden" id="starsinput" name="starsinput" value="{{$review->stars}}">

                    <div class="controls full">
                        <button class="full">Update review</button>
                    </div>
                </div>
                </div>
            </form>

        </div>
    </div>
@endsection