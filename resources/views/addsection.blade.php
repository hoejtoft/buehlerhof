@extends('master')
@section('title', 'Buehlerhof - add section')
@section('content')
    <div class="container container-top">
        <div class="row">

            <form method="post" enctype="multipart/form-data" action="{{url('addnewsection')}}">
                {{csrf_field()}}
                <h1 class="text-center Headline">Add new page section</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif

                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">Section content</h2>
                    <div class="controls third">
                        <input type="text" id="headline_de" value="{{ old('headline_de') }}" class="floatLabel" name="headline_de">
                        <label for="headline_de">German headline</label>
                    </div>
                    <div class="controls third">
                        <input type="text" id="headline_en" value="{{ old('headline_en') }}" class="floatLabel" name="headline_en">
                        <label for="headline_en">English headline</label>
                    </div>
                    <div class="controls third">
                        <input type="text" id="headline_it" value="{{ old('headline_it') }}" class="floatLabel" name="headline_it">
                        <label for="headline_it">Italian headline</label>
                    </div>
                    <div class="controls half">
                        <textarea name="sectiontxt_de"  class="floatLabel" id="sectiontxt_de">{{ old('sectiontxt_de') }}</textarea>
                        <label for="sectiontxt_de">German section text</label>
                    </div>
                    <div class="controls half">
                        <textarea name="alltxt_de" class="floatLabel" id="alltxt_de">{{ old('alltxt_de') }}</textarea>
                        <label for="alltxt_de">Write all the German text here</label>
                    </div>
                    <div class="controls half">
                        <textarea name="sectiontxt_en" class="floatLabel" id="sectiontxt_en">{{ old('sectiontxt_en') }}</textarea>
                        <label for="sectiontxt_en">English section text</label>
                    </div>
                    <div class="controls half">
                        <textarea name="alltxt_en" class="floatLabel" id="alltxt_en">{{ old('alltxt_en') }}</textarea>
                        <label for="alltxt_en">Write all the English text here</label>
                    </div>
                    <div class="controls half">
                        <textarea name="sectiontxt_it" class="floatLabel" id="sectiontxt_it">{{ old('sectiontxt_it') }}</textarea>
                        <label for="sectiontxt_it">Italian section text</label>
                    </div>
                    <div class="controls half">
                        <textarea name="alltxt_it" class="floatLabel" id="alltxt_it">{{ old('alltxt_it') }}</textarea>
                        <label for="alltxt_it">Write all the Italian text here</label>
                    </div>
                </div>
                <!--  Details -->
                <div class="form-group">
                    <h2 class="heading full">Section images</h2>

                    <div class="controls full">
                        <select class="floatLabel" name="isgallery" id="sectiontype">
                            <option value="blank"></option>
                            <option value="0">Single image</option>
                            <option value="1">Slider</option>
                        </select>
                        <label for="fruit">Slider or single image</label>
                    </div>
                    <div class="controls third">
                        <input type="file" name="imgname_1" id="imgname_1"  class="filestyle" style="display:none;">
                        <img width="100%" class="imgpreview" id="tempimg1" src="" style="display:none;">
                        <input type="hidden" name="imgurl_1" id="imgurl_1">
                        <div class="loader" id="loader_1" style="display: none;"></div>
                    </div>
                    <div class="controls third">
                        <input type="file" name="imgname_2" id="imgname_2" class="filestyle" style="display:none;">
                        <img width="100%" class="imgpreview" id="tempimg2" src="" style="display:none;">
                        <input type="hidden" name="imgurl_2" id="imgurl_2">
                        <div class="loader" id="loader_2" style="display: none;"></div>
                    </div>
                    <div class="controls third">
                        <input type="file" name="imgname_3" id="imgname_3" class="filestyle" style="display:none;">
                        <img width="100%" class="imgpreview" id="tempimg3" src="" style="display:none;">
                        <input type="hidden" name="imgurl_3" id="imgurl_3">
                        <div class="loader" id="loader_3" style="display: none;"></div>
                    </div>
                    <div class="controls half">
                        <input type="file" name="imgname_4" id="imgname_4" class="filestyle" style="display:none;">
                        <img width="100%" class="imgpreview" id="tempimg4" src="" style="display:none;">
                        <input type="hidden" name="imgurl_4" id="imgurl_4">
                        <div class="loader" id="loader_4" style="display: none;"></div>
                    </div>
                    <div class="controls half">
                        <input type="file" name="imgname_5" id="imgname_5" class="filestyle" style="display:none;">
                        <img width="100%" class="imgpreview" id="tempimg5" src="" style="display:none;">
                        <input type="hidden" name="imgurl_5" id="imgurl_5">
                        <div class="loader" id="loader_5" style="display: none;"></div>
                    </div>
                </div>
                <!--  More -->
                <div class="form-group">
                    <h2 class="heading full">Section settings</h2>
                    <div class="controls half">
                        <select class="floatLabel" name="category">
                            <option value="blank"></option>
                            <option value="Frontpage">Frontpage</option>
                            <option value="Hof">Hof</option>
                            <option value="Wohnen">Wohnen</option>
                            <option value="Urlauber">Urlauber</option>
                            <option value="Engagement">Engagement</option>
                            <option value="Kulinarik">Kulinarik</option>
                            <option value="Aktiv_sein">Aktiv sein</option>
                            <option value="Sydtirol">Südtirol</option>
                            <option value="Erde">Erde</option>
                            <option value="Wasser">Wasser</option>
                            <option value="Luft">Luft</option>
                            <option value="Feuer_Eis">Feuer und Eis</option>
                            <option value="Wellness">Wellness</option>
                            <option value="Anreise">Anreise</option>



                        </select>
                        <label for="fruit">Page category</label>
                    </div>
                    <div class="controls half">
                        <select class="floatLabel" name="type">
                            <option value="blank"></option>
                            <option value="0">Left</option>
                            <option value="1">Right</option>
                            <option value="2">Full width - no image</option>
                            <option value="3">Full width - image only</option>
                        </select>
                        <label for="fruit">Image position</label>
                    </div>
                    <div class="controls full">
                        <select class="floatLabel" name="buttonlink" id="buttonlink">
                            <option value="blank"></option>
                            <option value="/Erde">Erde</option>
                            <option value="/Wasser">Wasser</option>
                            <option value="/Luft">Luft</option>
                            <option value="/Feuereis">Feuereis</option>
                        </select>
                        <label for="buttonlink">Select button link</label>
                    </div>
                    <div class="controls full">
                        <div >Set button visibility</div>
                        <div class="switchToggle">
                            <input type="checkbox" id="switch">
                            <label for="switch">Toggle</label>
                            <input type="hidden" id="buttononnoff" name="buttonvisible">
                        </div>
                    </div>
                    <div class="controls full">
                        <button class="full">Add section</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection