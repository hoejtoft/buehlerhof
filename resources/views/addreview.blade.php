@extends('master')
@section('title', 'Buehlerhof - add review')
@section('content')
    <div class="container container-top">
        <div class="row">

            <form method="post" enctype="multipart/form-data" action="{{url('addnewreview')}}">
                {{csrf_field()}}
                <h1 class="text-center Headline">Add new review</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif

                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">Review content</h2>
                    <div class="controls full">
                        <input type="text" id="Sourcewebsite" value="{{ old('Sourcewebsite') }}" class="floatLabel" name="Sourcewebsite">
                        <label for="Sourcewebsite">Write review source website</label>
                    </div>

                    <div class="controls third">
                        <textarea name="reviewtext_de" class="floatLabel" id="reviewtext_de">{{ old('reviewtext_de') }}</textarea>
                        <label for="reviewtext_de">German review text</label>
                    </div>
                    <div class="controls third">
                        <textarea name="reviewtext_en" class="floatLabel" id="reviewtext_en">{{ old('reviewtext_en') }}</textarea>
                        <label for="reviewtext_en">English review text</label>
                    </div>
                    <div class="controls third">
                        <textarea name="reviewtext_it" class="floatLabel" id="reviewtext_it">{{ old('reviewtext_it') }}</textarea>
                        <label for="reviewtext_it">Italian review text</label>
                    </div>
                </div>
                <!--  Details -->
                <div class="form-group">
                    <h2 class="heading full">Review rating </h2>
                    <div class="rating">
                        <label>
                            <input type="radio" name="stars" value="1" />
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="2" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="3" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="4" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="5" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                    </div>
                    <input type="hidden" id="starsinput" name="starsinput">

                    <div class="controls full">
                        <button class="full">Add review</button>
                    </div>
                </div>
                </div>
            </form>
            <h2 class="heading full">Reviews</h2>
            <table class="table table-bordered table-striped custom-table-responsive">
                <tr>
                    <th>Source website</th>
                    <th>Reviewtext De</th>
                    <th>Reviewtext En</th>
                    <th>Reviewtext It</th>
                    <th>Stars</th>
                    <th>Created at:</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                @foreach($review as $row)
                    <tr>
                        <td>{{$row->sourcewebsite}}</td>
                        <td>{{$row->reviewtext_de}}</td>
                        <td>{{$row->reviewtext_en}}</td>
                        <td>{{$row->reviewtext_it}}</td>
                        <td>{{$row->stars}}</td>
                        <td>{{$row->created_at}}</td>
                        <td><a class="btn btn-warning" href="{{action('ReviewController@edit',$row->id)}}">Edit</a></td>
                        <td>
                            <form  method="post" class="delete_form reset-this" action="{{action('ReviewController@destroy', $row->id)}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE" />
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection