@extends('master')
@section('title', 'Buehlerhof - Wellness')
@section('content')
    <?php
            $lang = "";
            if(Session::has('lang')){
                $lang = Session::get('lang');
            }
            else{
                $lang = "de";
            }
    ?>
    {{--Content types--}}
    <section class="showcase">
        <div class="container-fluid p-0">
            @foreach($PageSection as $row)
                {{--Setting image array--}}
                <?php
                $images = array();
                if($row->imgname_1 != null){
                    array_push($images,$row->imgname_1);
                }
                if($row->imgname_2 != null){
                    array_push($images,$row->imgname_2);
                }
                if($row->imgname_3 != null){
                    array_push($images,$row->imgname_3);
                }
                if($row->imgname_4 != null){
                    array_push($images,$row->imgname_4);
                }
                if($row->imgname_5 != null){
                    array_push($images,$row->imgname_5);
                }
                ?>
            @if($row->type == 1)
                {{--Image right type--}}
                    <div class="row no-gutters pagesection">
                        @if($row->isgallery == 1)

                            <div class="col-lg-6 order-lg-2 text-white section-slider">
                            <div id="carouselExampleIndicators{{$row->id}}" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @for($i = 0; $i<count($images);$i++)
                                        <li data-target="#carouselExampleIndicators{{$row->id}}" data-slide-to="{{$i}}" class="slideindicator_{{$i}}"></li>
                                    @endfor
                                </ol>
                                <div class="carousel-inner">
                                    @for($i = 0; $i<count($images);$i++)
                                        <div class="carousel-item slideitem_{{$i}}">
                                            <img class="showcase-img" width="100%" src="{{$images[$i]}}" alt="First slide">
                                        </div>
                                    @endfor
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators{{$row->id}}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators{{$row->id}}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                                </div>
                        @else
                        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('{{ $row->imgname_1 }}');"></div>
                        @endif
                        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                            @if($lang == "en")
                            <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                            <h2>{{$row->headline_de}}</h2>
                            @else
                            <h2>{{$row->headline_it}}</h2>
                            @endif
                                <p class="lead mb-0">
                                    @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                    @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                    @else
                                    {{$row->sectiontxt_it}}
                                    @endif
                                </p>
                                @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
            @elseif($row->type == 0)
                    {{--Image left type--}}
                    <div class="row no-gutters pagesection">
                    @if($row->isgallery == 1)

                            <div class="col-lg-6 text-white section-slider">
                            <div id="carouselExampleIndicators{{$row->id}}" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @for($i = 0; $i<count($images);$i++)
                                        <li data-target="#carouselExampleIndicators{{$row->id}}" data-slide-to="{{$i}}" class="slideindicator_{{$i}}"></li>
                                    @endfor
                                </ol>
                                <div class="carousel-inner">
                                    @for($i = 0; $i<count($images);$i++)
                                        <div class="carousel-item slideitem_{{$i}}">
                                            <img class="showcase-img" width="100%" src="{{$images[$i]}}" alt="First slide">
                                        </div>
                                    @endfor
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators{{$row->id}}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators{{$row->id}}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                                </div>
                        @else
                        <div class="col-lg-6 text-white showcase-img" style="background-image: url('{{ $row->imgname_1 }}');"></div>
                        @endif
                        <div class="col-lg-6 my-auto showcase-text">
                            @if($lang == "en")
                                <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2>{{$row->headline_de}}</h2>
                            @else
                                <h2>{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead mb-0">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
                @else
                    {{--Full width type--}}
                    <div class="row no-gutters pagesection">
                        <div class="col-lg-12 my-auto showcase-text">
                            @if($lang == "en")
                                <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2>{{$row->headline_de}}</h2>
                            @else
                                <h2>{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead mb-0 text-center">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
            @endif
                    <!-- Modal -->
                    <div class="modal fade" id="modal_view{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        @if($lang == "en")
                                            {{$row->headline_en}}
                                        @elseif($lang == "de")
                                            {{$row->headline_de}}
                                        @else
                                            {{$row->headline_it}}
                                        @endif
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @if($lang == "en")
                                        {!!$row->alltxt_en!!}
                                    @elseif($lang == "de")
                                        {!!$row->alltxt_de!!}
                                    @else
                                        {!!$row->alltxt_it!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- End modal -->
            @endforeach

        </div>
    </section>
@endsection