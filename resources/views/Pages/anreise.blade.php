@extends('master')
@section('title', 'Buehlerhof')
@section('indexcontent')
    <?php
    $lang = "";
    if(Session::has('lang')){
        $lang = Session::get('lang');
    }
    else{
        $lang = "de";
    }
    ?>
    <section class="showcase">
        <div class="container-fluid p-0">
            @foreach($PageSection as $row)
                @if($row->type == 1)
                    <div class="row no-gutters pagesection">

                        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('{{ $row->imgname_1 }}');"></div>
                        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                            @if($lang == "en")
                                <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2>{{$row->headline_de}}</h2>
                            @else
                                <h2>{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead lead-p">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
                @elseif($row->type == 0)
                    <div class="row no-gutters pagesection">
                        <div class="col-lg-6 text-white showcase-img" style="background-image: url('{{ $row->imgname_1 }}');"></div>
                        <div class="col-lg-6 my-auto showcase-text">
                            @if($lang == "en")
                                <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2>{{$row->headline_de}}</h2>
                            @else
                                <h2>{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead lead-p">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
                @else
                    <div class="row no-gutters pagesection">
                        <div class="col-lg-12 my-auto showcase-text">
                            @if($lang == "en")
                                <h2>{{$row->headline_en}}</h2>
                            @elseif($lang == "de")
                                <h2>{{$row->headline_de}}</h2>
                            @else
                                <h2>{{$row->headline_it}}</h2>
                            @endif
                            <p class="lead text-center lead-p">
                                @if($lang == "en")
                                    {{$row->sectiontxt_en}}
                                @elseif($lang == "de")
                                    {{$row->sectiontxt_de}}
                                @else
                                    {{$row->sectiontxt_it}}
                                @endif
                            </p>
                            @if($row->hasbutton == 1)
                            <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                            @if($lang == "en")
                                    Read more
                                    @elseif($lang == "de")
                                    Weiterlesen
                                    @else
                                    Leggi di più
                            @endif
                            </div>
                            @endif
                        </div>
                        @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('PageController@destroy', $row->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('PageController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
                    </div>
                    @endif
                            <!-- Modal -->
                    <div class="modal fade" id="modal_view{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        @if($lang == "en")
                                            {{$row->headline_en}}
                                        @elseif($lang == "de")
                                            {{$row->headline_de}}
                                        @else
                                            {{$row->headline_it}}
                                        @endif
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @if($lang == "en")
                                        {{$row->alltxt_en}}
                                    @elseif($lang == "de")
                                        {{$row->alltxt_de}}
                                    @else
                                        {{$row->alltxt_it}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End modal -->
                    @endforeach

        </div>
    </section>
    <div class="row no-gutters">
    <div class="SecondaryContent">
    @foreach($secondpagecontent as $secondrow)
    @if($secondrow->type == 0)
        <div class="quarter">
            <div class="othercontent text-center">
                <h5>
                    @if($lang == "en")
                            {{$secondrow->headline_en}}
                        @elseif($lang == "de")
                            {{$secondrow->headline_de}}
                        @else
                            {{$secondrow->headline_it}}
                        @endif
                </h5>
                <p>
                @if($lang == "en")
                            {{$secondrow->sectiontxt_en}}
                        @elseif($lang == "de")
                            {{$secondrow->sectiontxt_de}}
                        @else
                            {{$secondrow->sectiontxt_it}}
                        @endif
                </p>
                @if($secondrow->hasbutton == 1)
                    <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$secondrow->id}}">
                        @if($lang == "en")
                            Read more
                        @elseif($lang == "de")
                            Weiterlesen
                        @else
                            Leggi di più
                        @endif
                    </div>
                @endif
            </div>
            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok other_content_crud">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SecondPageController@destroy', $secondrow->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$secondrow->id}}" href="{{action('SecondPageController@edit',$secondrow->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
        </div>

    @elseif($secondrow->type == 1)
        <div class="third">
            <div class="othercontent text-center">
                <h5>
                @if($lang == "en")
                            {{$secondrow->headline_en}}
                        @elseif($lang == "de")
                            {{$secondrow->headline_de}}
                        @else
                            {{$secondrow->headline_it}}
                        @endif
                </h5>
                <p>
                @if($lang == "en")
                            {{$secondrow->sectiontxt_en}}
                        @elseif($lang == "de")
                            {{$secondrow->sectiontxt_de}}
                        @else
                            {{$secondrow->sectiontxt_it}}
                        @endif
                </p>
                @if($secondrow->hasbutton == 1)
                    <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$row->id}}">
                        @if($lang == "en")
                            Read more
                        @elseif($lang == "de")
                            Weiterlesen
                        @else
                            Leggi di più
                        @endif
                    </div>
                @endif
            </div>
            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok other_content_crud">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SecondPageController@destroy', $secondrow->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$secondrow->id}}" href="{{action('SecondPageController@edit',$secondrow->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
        </div>
    @elseif($secondrow->type == 2)
        <div class="half">
            <div class="othercontent text-center">
                <h5>
                @if($lang == "en")
                            {{$secondrow->headline_en}}
                        @elseif($lang == "de")
                            {{$secondrow->headline_de}}
                        @else
                            {{$secondrow->headline_it}}
                        @endif
                </h5>
                <p>
                @if($lang == "en")
                            {{$secondrow->sectiontxt_en}}
                        @elseif($lang == "de")
                            {{$secondrow->sectiontxt_de}}
                        @else
                            {{$secondrow->sectiontxt_it}}
                        @endif
                </p>
                @if($secondrow->hasbutton == 1)
                    <div class="btn btn-readmore" data-toggle="modal" data-target="#modal_view{{$secondrow->id}}">
                        @if($lang == "en")
                            Read more
                        @elseif($lang == "de")
                            Weiterlesen
                        @else
                            Leggi di più
                        @endif
                    </div>
                @endif
            </div>
            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                        <div class="crud-blok other_content_crud">
                            <a>
                                <form  method="post" class="delete_form reset-this" action="{{action('SecondPageController@destroy', $secondrow->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />

                                    <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                </form>
                            </a>

                            <a class="btn btn-warning admincontrol" id="delete_{{$secondrow->id}}" href="{{action('SecondPageController@edit',$secondrow->id)}}"><i class="fa fa-edit"></i>Edit</a>
                        </div>
                        @endif
        </div>
    @endif
            <!-- Modal -->
                <div class="modal fade" id="modal_view{{$secondrow->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                    @if($lang == "en")
                                        {{$secondrow->headline_en}}
                                    @elseif($lang == "de")
                                        {{$secondrow->headline_de}}
                                    @else
                                        {{$secondrow->headline_it}}
                                    @endif
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                @if($lang == "en")
                                    {{$secondrow->alltxt_en}}
                                @elseif($lang == "de")
                                    {{$secondrow->alltxt_de}}
                                @else
                                    {{$secondrow->alltxt_it}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End modal -->
    @endforeach
    </div>
    </div>
    {{--end escond content--}}
    <div id="map"></div>
    <script>
        var map;
//        function initMap() {
//            map = new google.maps.Map(document.getElementById('map'), {
//                center: {lat: 46.7427795, lng: 11.65706545},
//                zoom: 14
//            });
//
//        }
        function initMap() {
            // The location of Uluru
            var pos = {lat: 46.7427795, lng: 11.65706545};
            // The map, centered at Uluru
            var map = new google.maps.Map(
                    document.getElementById('map'), {zoom: 14, center: pos});
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({position: pos, map: map});
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOzaHaTydR6PRX-nkiWcX6DBlrxpnGDS0&callback=initMap"
            async defer></script>

@endsection