@extends('master')
@section('title', 'Buehlerhof - Edit secondary section')
@section('content')
    <div class="container container-top">
        <div class="row">

            <form method="post" enctype="multipart/form-data" action="{{action('SecondPageController@update', $id)}}">
                {{csrf_field()}}
                {{ method_field('PUT')}}
                <h1 class="text-center Headline">Edit and update secondary section</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">Section content</h2>
                    <div class="controls third">
                        <input type="text" id="headline_de" value="{{ $pagesection->headline_de}}" class="floatLabel" name="headline_de">
                        <label for="headline_de">German headline</label>
                    </div>
                    <div class="controls third">
                        <input type="text" id="headline_en" value="{{ $pagesection->headline_en}}" class="floatLabel" name="headline_en">
                        <label for="headline_en">English headline</label>
                    </div>
                    <div class="controls third">
                        <input type="text" id="headline_it" value="{{ $pagesection->headline_it }}" class="floatLabel" name="headline_it">
                        <label for="headline_it">Italian headline</label>
                    </div>
                    <div class="controls half">
                        <textarea name="sectiontxt_de"  class="floatLabel" id="sectiontxt_de">{{ $pagesection->sectiontxt_de }}</textarea>
                        <label for="sectiontxt_de">German section text</label>
                    </div>
                    <div class="controls half">
                        <textarea name="alltxt_de" class="floatLabel" id="alltxt_de">{{ $pagesection->alltxt_de }}</textarea>
                        <label for="alltxt_de">Write all the German text here</label>
                    </div>
                    <div class="controls half">
                        <textarea name="sectiontxt_en" class="floatLabel" id="sectiontxt_en">{{ $pagesection->sectiontxt_en }}</textarea>
                        <label for="sectiontxt_en">English section text</label>
                    </div>
                    <div class="controls half">
                        <textarea name="alltxt_en" class="floatLabel" id="alltxt_en">{{ $pagesection->alltxt_en}}</textarea>
                        <label for="alltxt_en">Write all the English text here</label>
                    </div>
                    <div class="controls half">
                        <textarea name="sectiontxt_it" class="floatLabel" id="sectiontxt_it">{{ $pagesection->sectiontxt_it }}</textarea>
                        <label for="sectiontxt_it">Italian section text</label>
                    </div>
                    <div class="controls half">
                        <textarea name="alltxt_it" class="floatLabel" id="alltxt_it">{{ $pagesection->alltxt_it }}</textarea>
                        <label for="alltxt_it">Write all the Italian text here</label>
                    </div>
                </div>

                <!--  More -->
                <div class="form-group">
                    <h2 class="heading full">Section settings</h2>
                    <div class="controls half">
                        <select class="floatLabel" name="category">
                            <option value="blank"></option>
                            <option value="Frontpage" {{($pagesection->category == 'Frontpage') ? 'selected' : ''}}>Frontpage</option>
                            <option value="Hof" {{($pagesection->category == 'Hof') ? 'selected' : ''}}>Hof</option>
                            <option value="Wohnen" {{($pagesection->category == 'Wohnen') ? 'selected' : ''}}>Wohnen</option>
                            <option value="Urlauber" {{($pagesection->category == 'Urlauber') ? 'selected' : ''}}>Urlauber</option>
                            <option value="Engagement" {{($pagesection->category == 'Engagement') ? 'selected' : ''}}>Engagement</option>
                            <option value="Kulinarik" {{($pagesection->category == 'Kulinarik') ? 'selected' : ''}}>Kulinarik</option>
                            <option value="Aktiv_sein" {{($pagesection->category == 'Aktiv_sein') ? 'selected' : ''}}>Aktiv sein</option>
                            <option value="Südtirol" {{($pagesection->category == 'Südtirol') ? 'selected' : ''}}>Südtirol</option>
                            <option value="Erde" {{($pagesection->category == 'Erde') ? 'selected' : ''}}>Erde</option>
                            <option value="Wasser" {{($pagesection->category == 'Wasser') ? 'selected' : ''}}>Wasser</option>
                            <option value="Luft" {{($pagesection->category == 'Luft') ? 'selected' : ''}}>Luft</option>
                            <option value="Feuer_Eis" {{($pagesection->category == 'Feuer_Eis') ? 'selected' : ''}}>Feuer und Eis</option>
                            <option value="Wellness" {{($pagesection->category == 'Wellness') ? 'selected' : ''}}>Wellness</option>
                            <option value="Anreise" {{($pagesection->category == 'Anreise') ? 'selected' : ''}}>Service</option>
                        </select>
                        <label for="fruit">Page category</label>
                    </div>
                    <div class="controls half">
                        <select class="floatLabel" name="type">
                            <option value="blank"></option>
                            <option value="0" {{$pagesection->type == 0 ? "selected" : ""}}>Quarter</option>
                            <option value="1" {{$pagesection->type == 1 ? "selected" : ""}}>Third</option>
                            <option value="2" {{$pagesection->type == 2 ? "selected" : ""}}>Half</option>
                        </select>
                        <label for="fruit">Section type</label>
                    </div>
                    <div class="controls full">
                        <div >Set button visibility</div>
                        <div class="switchToggle">
                            <input type="checkbox" id="switch">
                            <label for="switch">Toggle</label>
                            <input type="hidden" id="buttononnoff" name="buttonvisible">
                        </div>
                        @if($pagesection->hasbutton == 1)
                            <script>
                                $(document).ready(function(){
                                    $("#buttononnoff").val(1);
                                    $("#switch").click();
                                });
                            </script>
                        @endif
                    </div>
                    <div class="controls full">
                        <button class="full">Update section</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection