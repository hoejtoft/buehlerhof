@extends('master')
@section('title', 'Buehlerhof - Manage slider')
@section('content')
    <div class="container container-top">
        <div class="row">

            <form method="post" enctype="multipart/form-data" action="{{url('addslideobj')}}">
                {{csrf_field()}}
                <h1 class="text-center Headline">Add & edit frontpage slider</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif

                <!--  General -->
                <div class="form-group">
                    <h2 class="heading full">Add new slider object</h2>

                    <div class="controls third">
                        <textarea name="teasertxt_de"  class="floatLabel" id="teasertxt_de">{{ old('teasertxt_de') }}</textarea>
                        <label for="teasertxt_de">German teaser text</label>
                    </div>
                    <div class="controls third">
                        <textarea name="teasertxt_en" class="floatLabel" id="teasertxt_en">{{ old('teasertxt_en') }}</textarea>
                        <label for="teasertxt_en">English teaser text</label>
                    </div>
                    <div class="controls third">
                        <textarea name="teasertxt_it" class="floatLabel" id="teasertxt_it">{{ old('teasertxt_it') }}</textarea>
                        <label for="teasertxt_it">Italian teaser text</label>
                    </div>
                    <div class="controls third">
                        <input type="text" name="buttontext_de" class="floatLabel" id="buttontext_de" value="{{ old('buttontext_de') }}">
                        <label for="buttontext_de">German button text</label>
                    </div>
                    <div class="controls third">
                        <input type="text" name="buttontext_en" class="floatLabel" id="buttontext_en" value="{{ old('buttontext_en') }}">
                        <label for="buttontext_en">English button text</label>
                    </div>
                    <div class="controls third">
                        <input type="text" name="buttontext_it" class="floatLabel" id="buttontext_it" value="{{ old('buttontext_it') }}">
                        <label for="buttontext_it">Italian button text</label>
                    </div>
                    <div class="controls full">
                        <div >Set text visibility</div>
                        <div class="switchToggle">
                            <input type="checkbox" id="switch">
                            <label for="switch">Toggle</label>
                            <input type="hidden" id="textonnoff" name="textvisible">
                        </div>

                    </div>
                    <div class="controls full">
                        <select class="floatLabel" name="buttonlink" id="buttonlink">
                            <option value="blank"></option>
                            <option value="/Hof">Hof</option>
                            <option value="/Engagement">Engagement</option>
                            <option value="/Urlauber">Urlauber</option>
                            <option value="/Wohnen">Wohnen</option>
                            <option value="/Kulinarik">Kulinarik</option>
                            <option value="/Aktivsein">Aktiv sein</option>
                            <option value="/Wellness">Wellness</option>
                            <option value="/Sydtyrol">Südtyrol</option>
                        </select>
                        <label for="buttonlink">Select button link</label>
                    </div>
                </div>
                <!--  Details -->
                <div class="form-group">
                    <h2 class="heading full">Add image</h2>

                    <div class="controls full">
                        <input type="file" name="imgname_1" id="imgname_1"  class="filestyle" style="display:block;">
                        <img width="100%" class="imgpreview" id="tempimg1" src="" style="display:none;">
                        <input type="hidden" name="imgurl_1" id="imgurl_1">
                        <div class="loader" id="loader_1" style="display: none;"></div>
                    </div>
                    <div class="controls full">
                        <button class="full">Add slider object</button>
                    </div>
                </div>

            </form>
        </div>
        <h2 class="heading full">Slider contents</h2>
        <table class="table table-bordered table-striped custom-table-responsive">
            <tr>
                <th>Imgurl</th>
                <th>teasertext De</th>
                <th>teasertext En</th>
                <th>teasertext It</th>
                <th>buttontext De</th>
                <th>buttontext En</th>
                <th>buttontext It</th>
                <th>buttonlink</th>
                <th>textvisible</th>
                <th>Created at:</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            @foreach($Slideobj as $row)
                <tr>
                    <td><img class="td_img" src="{{$row->imgurl}}"></td>
                    <td>{{$row->teasertext_de}}</td>
                    <td>{{$row->teasertext_en}}</td>
                    <td>{{$row->teasertext_it}}</td>
                    <td>{{$row->buttontext_de}}</td>
                    <td>{{$row->buttontext_en}}</td>
                    <td>{{$row->buttontext_it}}</td>
                    <td>{{$row->buttonlink}}</td>
                    <td>{{$row->textvisible}}</td>
                    <td>{{$row->created_at}}</td>
                    <td><a class="btn btn-warning" href="{{action('SlideController@edit',$row->id)}}">Edit</a></td>
                    <td>
                        <form  method="post" class="delete_form reset-this" action="{{action('SlideController@destroy', $row->id)}}">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE" />
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection