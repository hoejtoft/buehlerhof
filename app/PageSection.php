<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageSection extends Model
{
    protected $table = '_page_section';
    protected $fillable =
        ['category',
        'type',
        'headline_de',
        'headline_en',
        'headline_it',
        'sectiontxt_de',
        'sectiontxt_en',
        'sectiontxt_it',
        'alltxt_de',
        'alltxt_en',
        'alltxt_it',
        'isgallery',
        'imgname_1',
        'imgname_2',
        'imgname_3',
        'imgname_4',
        'imgname_5',
        'hasbutton',
        'buttonlink'
        ];
}
