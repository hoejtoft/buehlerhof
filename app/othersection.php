<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class othersection extends Model
{
    protected $table = '_second_page_section';
    protected $fillable =
            ['category',
            'type',
            'headline_de',
            'headline_en',
            'headline_it',
            'sectiontxt_de',
            'sectiontxt_en',
            'sectiontxt_it',
            'alltxt_de',
            'alltxt_en',
            'alltxt_it',
            'hasbutton'];
}
