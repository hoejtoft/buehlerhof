<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = '_service';
    protected $fillable =
        [
            'Service_headline_de',
            'Service_headline_en',
            'Service_headline_it',
            'Service_content_de',
            'Service_content_en',
            'Service_content_it',
        ];
}
