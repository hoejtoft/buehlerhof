<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class frontslider extends Model
{
    protected $table = 'frontslider';
    protected $fillable =
        ['imgurl',
            'teasertext_de',
            'teasertext_en',
            'teasertext_it',
            'buttontext_de',
            'buttontext_en',
            'buttontext_it',
            'textvisible',
            'buttonlink'];
}
