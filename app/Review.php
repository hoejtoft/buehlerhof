<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';
    protected $fillable =
            ['sourcewebsite',
            'reviewtext_de',
            'reviewtext_en',
            'reviewtext_it',
            'stars'];
}
