<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReviewController extends Controller
{
    public function addReview(){
        $review = DB::table('review')->get()->toArray();
        return view('addreview',compact('review'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'Sourcewebsite' => 'required',
            'reviewtext_de' => 'required',
            'reviewtext_en' => 'required',
            'reviewtext_it' => 'required',
            'starsinput' => 'required',
        ]);


        try{

            $review = new Review([
                'sourcewebsite' => $request->get('Sourcewebsite'),
                'reviewtext_de' => $request->get('reviewtext_de'),
                'reviewtext_en' => $request->get('reviewtext_en'),
                'reviewtext_it' => $request->get('reviewtext_it'),
                'stars' => $request->get('starsinput'),
            ]);
            $review->save();
//                \Session::flash('success', 'New page section was created' );
            return redirect("/addreview")->with('success','New review was created');




        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }
    }
    public function edit($id)
    {
//        if(auth()->guest()){
//            return redirect("/products/Wallhangers");
//        }
        $review = Review::find($id);
        return view('editreview',compact('review','id'));
    }
    public function update(Request $request,$id)
    {

        $this->validate($request,[
            'Sourcewebsite' => 'required',
            'reviewtext_de' => 'required',
            'reviewtext_en' => 'required',
            'reviewtext_it' => 'required',
            'starsinput' => 'required',
        ]);

        $review = Review::find($id);
        try{


            $review->sourcewebsite = $request->get('Sourcewebsite');
            $review->reviewtext_de = $request->get('reviewtext_de');
            $review->reviewtext_en = $request->get('reviewtext_en');
            $review->reviewtext_it = $request->get('reviewtext_it');
            $review->stars = $request->get('starsinput');


            $review->save();


                return redirect("/addreview")->with('success','New page section was created');
        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function destroy($id)
    {

        $review = Review::find($id);

        $review->delete();
            return redirect("/addreview");


    }
}
