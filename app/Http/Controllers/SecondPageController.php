<?php

namespace App\Http\Controllers;

use App\othersection;
use Illuminate\Http\Request;


class SecondPageController extends Controller
{
    public function addOtherSection(){
        return view('addother');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'headline_de' => 'required',
            'headline_en' => 'required',
            'headline_it' => 'required',
            'sectiontxt_de' => 'required',
            'sectiontxt_en' => 'required',
            'sectiontxt_it' => 'required',
            'alltxt_de' => 'required',
            'alltxt_en' => 'required',
            'alltxt_it' => 'required',
            'category' => 'required',
            'type' => 'required',
        ]);


        try{

            $pagesection = new othersection([
                'headline_de' => $request->get('headline_de'),
                'headline_en' => $request->get('headline_en'),
                'headline_it' => $request->get('headline_it'),
                'sectiontxt_de' => $request->get('sectiontxt_de'),
                'sectiontxt_en' => $request->get('sectiontxt_en'),
                'sectiontxt_it' => $request->get('sectiontxt_it'),
                'alltxt_de' => $request->get('alltxt_de'),
                'alltxt_en' => $request->get('alltxt_en'),
                'alltxt_it' => $request->get('alltxt_it'),
                'category' => $request->get('category'),
                'type' => $request->get('type'),
                'hasbutton' => $request->get('buttonvisible'),
            ]);
            $pagesection->save();
//                \Session::flash('success', 'New page section was created' );
            return redirect("/addother")->with('success','New page section was created');




        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function edit($id)
    {
//        if(auth()->guest()){
//            return redirect("/products/Wallhangers");
//        }
        $pagesection = othersection::find($id);
        return view('Pages.editother',compact('pagesection','id'));
    }
    public function update(Request $request,$id)
    {

        $this->validate($request,[
            'headline_de' => 'required',
            'headline_en' => 'required',
            'headline_it' => 'required',
            'sectiontxt_de' => 'required',
            'sectiontxt_en' => 'required',
            'sectiontxt_it' => 'required',
            'alltxt_de' => 'required',
            'alltxt_en' => 'required',
            'alltxt_it' => 'required',
            'category' => 'required',
            'type' => 'required',
        ]);

        $pagesection = othersection::find($id);
        try{


            $pagesection->headline_de = $request->get('headline_de');
            $pagesection->headline_en = $request->get('headline_en');
            $pagesection->headline_it = $request->get('headline_it');
            $pagesection->sectiontxt_de = $request->get('sectiontxt_de');
            $pagesection->sectiontxt_en = $request->get('sectiontxt_en');
            $pagesection->sectiontxt_it = $request->get('sectiontxt_it');
            $pagesection->alltxt_de = $request->get('alltxt_de');
            $pagesection->alltxt_en = $request->get('alltxt_en');
            $pagesection->alltxt_it = $request->get('alltxt_it');
            $pagesection->category = $request->get('category');
            $pagesection->type = $request->get('type');
            $pagesection->hasbutton = $request->get('buttonvisible');

            $pagesection->save();

//                \Session::flash('success', 'New page section was created' );
            if($pagesection->category == "Frontpage"){
                return redirect("/");
            }
            else{
                return redirect("/".$pagesection->category)->with('success','New page section was created');
            }




        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function destroy($id)
    {

        $pagesection = othersection::find($id);

        $pagesection->delete();
        if($pagesection->category == "Frontpage"){
            return redirect("/");
        }
        else{
            return redirect("/".$pagesection->category)->with('success','section deleted');

        }
    }
}
