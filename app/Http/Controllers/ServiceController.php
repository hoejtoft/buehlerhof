<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    public function addservicesection(){
        $Servicesection = DB::table('_service')->get()->toArray();
        return view('addservicesection',compact('Servicesection'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'Service_headline_de' => 'required',
            'Service_headline_en' => 'required',
            'Service_headline_it' => 'required',
            'Service_content_de' => 'required',
            'Service_content_en' => 'required',
            'Service_content_it' => 'required',
        ]);


        try{

            $servicesection = new Service([
                'Service_headline_de' => $request->get('Service_headline_de'),
                'Service_headline_en' => $request->get('Service_headline_en'),
                'Service_headline_it' => $request->get('Service_headline_it'),
                'Service_content_de' => $request->get('Service_content_de'),
                'Service_content_en' => $request->get('Service_content_en'),
                'Service_content_it' => $request->get('Service_content_it'),
            ]);
            $servicesection->save();
//                \Session::flash('success', 'New page section was created' );
            return redirect("/addservicesection")->with('success','New page section was created');

        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function edit($id)
    {
//        if(auth()->guest()){
//            return redirect("/products/Wallhangers");
//        }
        $servicesection = Service::find($id);
        return view('editservicesection',compact('servicesection','id'));
    }
    public function update(Request $request,$id)
    {

        $this->validate($request,[
            'Service_headline_de' => 'required',
            'Service_headline_en' => 'required',
            'Service_headline_it' => 'required',
            'Service_content_de' => 'required',
            'Service_content_en' => 'required',
            'Service_content_it' => 'required',
        ]);

        $servicesection = Service::find($id);
        try{


            $servicesection->Service_headline_de = $request->get('Service_headline_de');
            $servicesection->Service_headline_en = $request->get('Service_headline_en');
            $servicesection->Service_headline_it = $request->get('Service_headline_it');
            $servicesection->Service_content_de = $request->get('Service_content_de');
            $servicesection->Service_content_en = $request->get('Service_content_en');
            $servicesection->Service_content_it = $request->get('Service_content_it');

            $servicesection->save();


            return redirect("/addservicesection")->with('success','New page section was created');
        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function destroy($id)
    {

        $servicesection = Service::find($id);

        $servicesection->delete();
        return redirect("/addservicesection");


    }
}
