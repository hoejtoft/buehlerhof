<?php

namespace App\Http\Controllers;

use App\frontpageslider;
use App\frontslider;
use App\test;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,[
            'teasertxt_de' => 'required',
            'teasertxt_en' => 'required',
            'teasertxt_it' => 'required',
            'buttontext_de' => 'required',
            'buttontext_en' => 'required',
            'buttontext_it' => 'required',
            'buttonlink' => 'required',
            'imgurl_1' => 'required',
        ]);

        try{

            $slideobj = new frontslider([
                'imgurl' => $request->get('imgurl_1'),
                'teasertext_de' => $request->get('teasertxt_de'),
                'teasertext_en' => $request->get('teasertxt_en'),
                'teasertext_it' => $request->get('teasertxt_it'),
                'buttontext_de' => $request->get('buttontext_de'),
                'buttontext_en' => $request->get('buttontext_en'),
                'buttontext_it' => $request->get('buttontext_it'),
                'buttonlink' => $request->get('buttonlink'),
                'textvisible' => $request->get('textvisible'),

            ]);

            $slideobj->save();
//                \Session::flash('success', 'New page section was created' );
            return redirect("/editslider")->with('success','New page section was created');




        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }
    }
    public function edit($id)
    {
        $slideobj = frontslider::find($id);
        return view('editslider',compact('slideobj','id'));
    }
    public function destroy($id)
    {
        $slideobj =frontslider::find($id);
        $slideobj->delete();
        return redirect("/editslider")->with('success','Slide deleted');
        //return redirect()->route('message.index')->with('success','Post deleted');
    }
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'teasertext_de' => 'required',
            'teasertext_en' => 'required',
            'teasertext_it' => 'required',
            'buttontext_de' => 'required',
            'buttontext_en' => 'required',
            'buttontext_it' => 'required',
            'buttonlink' => 'required',
            'imgurl_1' => 'required',
        ]);
        $slideobj = frontslider::find($id);
        $slideobj->teasertext_de = $request->get('teasertext_de');
        $slideobj->teasertext_en = $request->get('teasertext_en');
        $slideobj->teasertext_it = $request->get('teasertext_it');
        $slideobj->buttontext_de = $request->get('buttontext_de');
        $slideobj->buttontext_en = $request->get('buttontext_en');
        $slideobj->buttontext_it = $request->get('buttontext_it');
        $slideobj->buttonlink = $request->get('buttonlink');
        $slideobj->imgurl = $request->get('imgurl_1');
        $slideobj->textvisible = $request->get('textvisible');
        $slideobj->save();
        //return redirect()->route('message.index')->with('success','Data updated');
        return redirect("/editslider")->with('success','Data updated');
    }
}
