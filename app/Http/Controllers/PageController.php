<?php

namespace App\Http\Controllers;

use App\PageSection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;


class PageController extends Controller
{
    public function addSection(){
        return view('addsection');
    }
    public function setEnlang(){
        \Session::put('lang', "en" );
        return redirect::back();
    }
    public function setDelang(){
        \Session::put('lang', "de");
        return redirect::back();
    }
    public function setItlang(){
        \Session::put('lang', "it");
        return redirect::back();
    }
    public function getlang(){
        $test = Session::get('lang');
        return $test;
    }
    public function editslider(){
        $Slideobj = DB::table('frontslider')->get()->toArray();
        return view('addeditslider',compact('Slideobj'));
    }

    public function displayHof()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Hof')->get()->toArray();
        return view('Pages.hof',compact('PageSection'));
    }
    public function displayEngagement()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Engagement')->get()->toArray();
        return view('Pages.engagement',compact('PageSection'));
    }
    public function displayUrlaub()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Urlauber')->get()->toArray();
        return view('Pages.urlaub',compact('PageSection'));
    }
    public function displayWohnen()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Wohnen')->get()->toArray();
        return view('Pages.wohnen',compact('PageSection'));
    }
    public function displayKulinarik()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Kulinarik')->get()->toArray();
        return view('Pages.kulinarik',compact('PageSection'));
    }
    public function displayWellness()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Wellness')->get()->toArray();
        return view('Pages.wellness',compact('PageSection'));
    }
    public function displaySouthtyrol()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Sydtirol')->get()->toArray();
        return view('Pages.southtyrol',compact('PageSection'));
    }
    public function displayAktivsein()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Aktiv_sein')->get()->toArray();
        return view('Pages.aktivsein',compact('PageSection'));
    }
    public function displayErde()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Erde')->get()->toArray();
        $secondpagecontent = DB::table('_second_page_section')->where('category', '=', 'Erde')->get()->toArray();
        return view('Pages.erde',compact('secondpagecontent','PageSection'));
    }
    public function displayWasser()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Wasser')->get()->toArray();
        $secondpagecontent = DB::table('_second_page_section')->where('category', '=', 'Wasser')->get()->toArray();
        return view('Pages.wasser',compact('secondpagecontent','PageSection'));
    }
    public function displayLuft()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Luft')->get()->toArray();
        $secondpagecontent = DB::table('_second_page_section')->where('category', '=', 'Luft')->get()->toArray();

        return view('Pages.luft',compact('secondpagecontent','PageSection'));
    }
    public function displayFeuerEis()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Feuer_Eis')->get()->toArray();
        $secondpagecontent = DB::table('_second_page_section')->where('category', '=', 'Feuer_Eis')->get()->toArray();
        return view('Pages.feuereis',compact('secondpagecontent','PageSection'));
    }

    public function displayAnreise(){
        $PageSection = DB::table('_page_section')->where('category', '=', 'Anreise')->get()->toArray();
        $secondpagecontent = DB::table('_second_page_section')->where('category', "=", "Anreise")->get()->toArray();
        return view('Pages.anreise',compact('PageSection','secondpagecontent'));

    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'headline_de' => 'required',
            'headline_en' => 'required',
            'headline_it' => 'required',
            'sectiontxt_de' => 'required',
            'sectiontxt_en' => 'required',
            'sectiontxt_it' => 'required',
            'alltxt_de' => 'required',
            'alltxt_en' => 'required',
            'alltxt_it' => 'required',
            'isgallery' => 'required',
            'imgurl_1' => 'required',
            'category' => 'required',
            'type' => 'required',
        ]);


        try{

                $pagesection = new PageSection([
                    'headline_de' => $request->get('headline_de'),
                    'headline_en' => $request->get('headline_en'),
                    'headline_it' => $request->get('headline_it'),
                    'imgname_1' => $request->get('imgurl_1'),
                    'imgname_2' => $request->get('imgurl_2'),
                    'imgname_3' => $request->get('imgurl_3'),
                    'imgname_4' => $request->get('imgurl_4'),
                    'imgname_5' => $request->get('imgurl_5'),
                    'sectiontxt_de' => $request->get('sectiontxt_de'),
                    'sectiontxt_en' => $request->get('sectiontxt_en'),
                    'sectiontxt_it' => $request->get('sectiontxt_it'),
                    'alltxt_de' => $request->get('alltxt_de'),
                    'alltxt_en' => $request->get('alltxt_en'),
                    'alltxt_it' => $request->get('alltxt_it'),
                    'category' => $request->get('category'),
                    'type' => $request->get('type'),
                    'isgallery' => $request->get('isgallery'),
                    'hasbutton' => $request->get('buttonvisible'),
                    'buttonlink' => $request->get('buttonlink'),
                ]);
                $pagesection->save();
//                \Session::flash('success', 'New page section was created' );
                return redirect("/addsection")->with('success','New page section was created');




        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function update(Request $request,$id)
    {

        $this->validate($request,[
            'headline_de' => 'required',
            'headline_en' => 'required',
            'headline_it' => 'required',
            'sectiontxt_de' => 'required',
            'sectiontxt_en' => 'required',
            'sectiontxt_it' => 'required',
            'alltxt_de' => 'required',
            'alltxt_en' => 'required',
            'alltxt_it' => 'required',
            'isgallery' => 'required',
            'imgurl_1' => 'required',
            'category' => 'required',
            'type' => 'required',
        ]);

        $pagesection = PageSection::find($id);
        try{


            $pagesection->headline_de = $request->get('headline_de');
            $pagesection->headline_en = $request->get('headline_en');
            $pagesection->headline_it = $request->get('headline_it');
            $pagesection->imgname_1 = $request->get('imgurl_1');
            $pagesection->imgname_2 = $request->get('imgurl_2');
            $pagesection->imgname_3 = $request->get('imgurl_3');
            $pagesection->imgname_4 = $request->get('imgurl_4');
            $pagesection->imgname_5 = $request->get('imgurl_5');
            $pagesection->sectiontxt_de = $request->get('sectiontxt_de');
            $pagesection->sectiontxt_en = $request->get('sectiontxt_en');
            $pagesection->sectiontxt_it = $request->get('sectiontxt_it');
            $pagesection->alltxt_de = $request->get('alltxt_de');
            $pagesection->alltxt_en = $request->get('alltxt_en');
            $pagesection->alltxt_it = $request->get('alltxt_it');
            $pagesection->category = $request->get('category');
            $pagesection->type = $request->get('type');
            $pagesection->isgallery = $request->get('isgallery');
            $pagesection->hasbutton = $request->get('buttonvisible');
            $pagesection->buttonlink = $request->get('buttonlink');

            $pagesection->save();

//                \Session::flash('success', 'New page section was created' );
            if($pagesection->category == "Frontpage"){
                return redirect("/");
            }
            elseif($pagesection->category == "Aktiv_sein"){
                return redirect("/Aktivsein");
            }
            else{
                return redirect("/".$pagesection->category)->with('success','New page section was created');
            }




        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function edit($id)
    {
//        if(auth()->guest()){
//            return redirect("/products/Wallhangers");
//        }
        $pagesection = PageSection::find($id);
        return view('Pages.edit',compact('pagesection','id'));
    }
    public function destroy($id)
    {

        $pagesection = PageSection::find($id);

        $pagesection->delete();
        return redirect("/".$pagesection->category)->with('success','section deleted');
    }
}
