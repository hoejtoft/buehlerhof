<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $PageSection = DB::table('_page_section')->where('category', '=', 'Frontpage')->get()->toArray();
        $Sliderobj = DB::table('frontslider')->get()->toArray();
        $secondpagecontent = DB::table('_second_page_section')->where('category', "=", "Frontpage")->get()->toArray();
        $Reviews = DB::table('review')->get()->toArray();
        return view('index',compact('PageSection','Sliderobj','secondpagecontent','Reviews'));
    }

    public function checkDBCon(){
        try {
            DB::connection()->getPdo();
            return "connected to: ". DB::connection()->getDatabaseName();
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }
}
