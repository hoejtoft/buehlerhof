<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get("/", "HomeController@index");
Route::get('/db', 'HomeController@checkDBCon');
Route::get('/addreview', 'ReviewController@addReview');
Route::get('/addservicesection', 'ServiceController@addservicesection');
Route::get('/addsection', 'PageController@addSection');
Route::get('/addother', 'SecondPageController@addOtherSection');
Route::get('/Hof', 'PageController@displayHof');
Route::get('/Engagement', 'PageController@displayEngagement');
Route::get('/Urlaub', 'PageController@displayUrlaub');
Route::get('/Wohnen', 'PageController@displayWohnen');
Route::get('/Kulinarik', 'PageController@displayKulinarik');
Route::get('/Wellness', 'PageController@displayWellness');
Route::get('/Sydtirol', 'PageController@displaySouthtyrol');
Route::get('/Aktivsein', 'PageController@displayAktivsein');
Route::get('/Erde', 'PageController@displayErde');
Route::get('/Wasser', 'PageController@displayWasser');
Route::get('/Luft', 'PageController@displayLuft');
Route::get('/Feuereis', 'PageController@displayFeuerEis');
Route::get('/Anreise', 'PageController@displayAnreise');
Route::get('/en', 'PageController@setEnlang');
Route::get('/de', 'PageController@setDelang');
Route::get('/it', 'PageController@setItlang');
Route::get('/getlang', 'PageController@getlang');
Route::get('/editslider', 'PageController@editslider');
//Route::get('/addslideobj', 'PageController@addslideobj');
Route::resource('addnewsection','PageController');
Route::resource('addothersection','SecondPageController');
Route::resource('addslideobj','SlideController');
Route::resource('addnewreview','ReviewController');
Route::resource('addservice','ServiceController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
